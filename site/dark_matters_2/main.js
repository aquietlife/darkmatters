document.addEventListener('DOMContentLoaded', function() {
    const scene = document.querySelector('a-scene');
    const landing = document.querySelector('#landing');
    const loading = document.querySelector('#landing .loading');
    const startButton = document.querySelector('#landing .start-btn');

    const activateSounds = () => {
     const sounds = document.querySelectorAll('[sound]')
       sounds.forEach((soundEl) => {
           soundEl.components.sound.playSound();
       })
   };

    scene.addEventListener('loaded', function (e) {
        setTimeout(() => {
            loading.style.display = 'none';
            landing.style.backgroundColor = 'rgba(22, 22, 29, 0.8)';
            startButton.style.opacity = 1;
        }, 50);
    });

    startButton.addEventListener('click', function (e) {
        activateSounds();
        landing.style.display = 'none';
    });
});

AFRAME.registerComponent('spectrogram', {
        init: function () {
          this.el.addEventListener('model-loaded', () => {
            const obj = this.el.getObject3D('mesh');
            obj.traverse(node => {
              if (node.isMesh){
                  let texture = new THREE.TextureLoader().load( "../assets/texture/deepfake_bird.png");
                  texture.encoding = THREE.sRGBEncoding;
                  node.material = new THREE.MeshStandardMaterial({
                    map: texture,
                    color: '0xFFFFFF'
                  });

              }
            });
          });
        }
      });

// AFRAME.registerComponent('audio-toggle', {
//   schema: {
//     playing: {
//       type: 'boolean',
//       default: false
//     }
//   },
//   update: function() {
//     let data = this.data
//     let el = this.el
//     let playing = data.playing
//
//     if (playing === true) {
//       el.components.sound.playSound()
//     }
//     el.addEventListener('click', () => {
//       if (playing) {
//         el.components.sound.pauseSound()
//         playing = false
//       } else {
//         el.components.sound.playSound()
//         playing = true
//       }
//     })
//   }
// })
