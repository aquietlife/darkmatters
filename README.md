# Dark Matters

## Installation

To install requirements necessary to run the notebooks:

- Install anaconda

- Clone this repo

- Install required packages:

`conda create -n darkmatters --file package-list.txt`

## Running

For running the notebooks, run:

`conda activate darkmatters`

`jupyter notebook`
